import os, sys
sys.path.append(os.getcwd())

import torch
import numpy as np
import torch.backends.cudnn as cudnn
import pynvml

import itertools
from os.path import join as pathjoin
from copy import deepcopy
from collections import OrderedDict

from config import Config # pylint: disable=no-name-in-module

conf = Config()

def cartesian_product_itertools(arrays):
    return np.array(list(itertools.product(*arrays)))

def gaussian_random_direction(sd, random_seed=None):
    """Create a random directional "vector" for the neural net.
    """
    if random_seed is not None:
        torch.random.manual_seed(random_seed)
    sd_rand = deepcopy(sd)
    for opr in sd:
        print(opr, '\t', sd[opr].shape)
        sd_rand[opr] = torch.empty_like(sd[opr]).normal_() # pylint: disable=maybe-no-member

    return sd_rand

def gaussian_random_direction_filterwise_rescaled(sd: OrderedDict, fc_rescale=True, bias_rescale=True, random_seed=np.random.randint(10000)):
    """Create a rescaled random directional "vector" for the neural net.
    The weight for conv layers are rescaled filterwise as decribed in https://arxiv.org/pdf/1712.09913.pdf.
    As proposed in the paper, weight of fc layers and biases should also be rescaled, but they can be disabled in this function.
    We skip bn in this case.
    """

    sd_rand = deepcopy(sd)
    if random_seed is not None:
        torch.random.manual_seed(random_seed)
    for opr in sd:
        # print(opr, '\t', sd[opr].shape)
        # print(sd[opr].shape, opr, len(sd[opr].shape))
        # print(sd[opr].type())
        if len(sd[opr].shape) == 0:
            sd_rand[opr] = deepcopy(sd[opr])
        else:
            sd_rand[opr] = torch.empty_like(sd[opr]).normal_(std=1) # pylint: disable=maybe-no-member

        # Rescale conv and fc weights (nchw for conv)
        if ('conv' in opr and '.weight' in opr and len(sd[opr].shape) == 4) or ('fc' in opr and '.weight' in opr and len(sd[opr].shape) == 2 and fc_rescale):
            for i in range(len(sd[opr])):
                base_filter, gaussian_rand_filter = sd[opr][i], sd_rand[opr][i]
                sd_rand[opr][i] = gaussian_rand_filter * torch.norm(base_filter) / torch.norm(gaussian_rand_filter)
                # print(torch.norm(base_filter), torch.norm(gaussian_rand_filter))
        
        # Rescale biases
        if bias_rescale:
            if '.bias' in opr:
                base_bias, gaussian_rand_bias = sd[opr], sd_rand[opr]
                assert base_bias.dim() == 1, 'awkward dimension'
                sd_rand[opr] = gaussian_rand_bias * torch.norm(base_bias) / torch.norm(gaussian_rand_bias)

    return sd_rand

def state_dict_add(a: OrderedDict, b: OrderedDict):

    c = deepcopy(a)
    assert a.keys() == b.keys(), 'network mismatch'
    for opr in c:
        assert c[opr].shape == b[opr].shape, 'shape of {} mismatch'.format(opr)
        if len(c[opr].shape) >= 1: # No long type tensor
            c[opr] += b[opr]
    return c

def state_dict_scalar_mul(a: OrderedDict, b: float):

    c = deepcopy(a)
    for opr in c:
        if len(c[opr].shape) >= 1: # No long type tensor
            c[opr] *= b
    return c

def prepare_net(net, use_gpu=True):

    handle = None
    device = 'cpu'
    if not use_gpu:
        print('Running on CPUs')
        return net, device, handle
    
    if torch.cuda.is_available():
        device = 'cuda'

    if device != 'cpu':
        print('Running on GPU')
        net = net.to(device)
        # net = torch.nn.DataParallel(net)
        cudnn.benchmark = True
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        print(pynvml.nvmlDeviceGetName(handle))
    else:
        print('No CUDA devices available, run on CPUs')
    
    return net, device, handle

if __name__ == "__main__":

    device = 'cpu'
    if torch.cuda.is_available():
        device = 'cuda'
        print('cuda initiated') 

    net = conf.net()
    net = net.to(device)
    
    net.load_state_dict(torch.load(pathjoin(conf.model_path, 'final.pth')))
    net_state_dict = net.state_dict()