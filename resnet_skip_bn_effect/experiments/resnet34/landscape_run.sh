#!/bin/bash
echo "Running landscape on run#" $1

python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization.py -run=$(($1)) -m=final.pth -r=3 -d=15 -i -w"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization.py -run=$(($1)) -m=final.pth -r=1 -d=15 -i -w"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization.py -run=$(($1)) -m=final.pth -r=0.3 -d=15 -i -w"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization.py -run=$(($1)) -m=final.pth -r=0.1 -d=15 -i -w"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization.py -run=$(($1)) -m=final.pth -r=0.01 -d=15 -i -w"