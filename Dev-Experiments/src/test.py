import os, sys, json
sys.path.append(os.getcwd())

import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import pynvml

from config import Config # pylint: disable=no-name-in-module
import utils
import algos.pyhessian as pyhessian
import hessian_eigenthings

import argparse
conf = Config()

def test_model(network, dataloader, criterion, device, progress_bar=False):

    enumerator = enumerate(dataloader, 0)
    if progress_bar:
        enumerator = tqdm(enumerator, total=len(dataloader), miniters=int(len(dataloader)/100))
    network.zero_grad()

    loss = 0.0
    corrected, total = 0, 0

    for _, data in enumerator:

        inputs, labels = data
        if device != 'cpu':
            inputs, labels = inputs.to(device), labels.to(device)

        outputs = network(inputs)
        loss += criterion(outputs, labels).item()
        _, predicted = torch.Tensor.max(outputs.data, 1)
        corrected += (predicted == labels).sum().item()
        total += labels.size(0)
    
    return {'loss': loss / len(dataloader), 'acc': corrected / total}

def get_train_error(net, device):

    train_set = conf.dataset(train=True, transform=conf.test_transform)
    train_loader = DataLoader(train_set, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)
    train_eval_result = test_model(net, train_loader, conf.criterion, device, progress_bar=False)
    return train_eval_result

def get_test_error(net, device):

    test_set = conf.dataset(train=False, transform=conf.test_transform)
    test_loader = DataLoader(test_set, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)
    test_eval_result = test_model(net, test_loader, conf.criterion, device, progress_bar=False)
    return test_eval_result

def calculate_hessian_eigenvalue():
    return

def main():

    global conf

    parser = argparse.ArgumentParser()
    parser.add_argument('-run', type=int, help='The i-th run of the experiment (default=1)', default=1)
    parser.add_argument('-m', type=str, default='final.pth', help='the .pth state dict model for testing')
    args = parser.parse_args()

    conf = Config(run_number=args.run)
    print(conf.model_path)

    exit()
    net, device, _ = utils.prepare_net(conf.net(), conf.use_gpu)
    
    model_path = args.m
    model_path = os.path.join(conf.model_path, model_path)
    assert os.path.isfile(model_path), 'invalid model path'
    net.load_state_dict(torch.load(model_path))
    print('Network state loaded.')

    exp_meta_path = os.path.join(conf.experiment_path, 'exp_log.json')
    meta = dict()
    if os.path.isfile(exp_meta_path):
        meta_io = open(exp_meta_path, 'r')
        meta = json.load(meta_io)
        meta_io.close()

    print('evaluating results')
    train_error_last = get_train_error(net, device)
    test_error_last = get_test_error(net, device)
    meta[os.path.join(conf.model_path, 'final.pth')] = {'test_err': test_error_last, 'train_err': train_error_last, 'generalization_gap': {k: test_error_last[k] - train_error_last[k] for k in test_error_last}}

    meta_writer = open(exp_meta_path, 'w')
    json.dump(meta, meta_writer)

if __name__ == "__main__":
    main()