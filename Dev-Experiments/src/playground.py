import os, sys, json
sys.path.append(os.getcwd())

import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import datetime

from config import Config # pylint: disable=no-name-in-module
import utils
import algos.pyhessian as pyhessian
import hessian_eigenthings

from IPython import embed

def hessian_comp_main():

    conf = Config(run_number=1)
    net, device, _ = utils.prepare_net(conf.net(), conf.use_gpu)
    init_model_path = './experiment_log/run_1/models/init.pth'
    yikai_path = '/usr/xtmp/CSPlus/VOLDNN/Yikai/runs/1/models/final.pth'
    stable_path = '/usr/xtmp/CSPlus/VOLDNN/Xingyu/train_log/preliminary_exp/cifar10_resnets/resnet34vanilla_SGD_bt128_lrmodified_tfmodified_noaug/models/final.pth'
    net.load_state_dict(torch.load(yikai_path))

    test_set = conf.dataset(train=True, transform=conf.transforms.naive_transform)
    test_loader = DataLoader(test_set, batch_size=conf.testing_batchsize, shuffle=False, num_workers=conf.test_provider_count)

    start_time = datetime.datetime.now()
    # print("xy stable power")
    print("yikai stable power")
    # eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, test_loader, conf.criterion, 2, mode="lanczos")
    eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, test_loader, conf.criterion, 2)
    print(eigenvals)
    print(eigenvecs)
    # torch.save(eigenvecs, 'eigenvecs_xyreal_power_1.tmp')
    print(datetime.datetime.now() - start_time)


def network_base():
    conf = Config(run_number=1)
    eigenvecs = torch.load('eigenvecs.tmp')
    print(eigenvecs)
    eigen_vec_1 = torch.tensor(eigenvecs[0]) # pylint: disable=not-callable
    print(eigen_vec_1.size())

    net = conf.net()
    params = net.parameters()
    sizes = []
    length = 0
    for i, x in enumerate(params):
        print(i, x.shape)
        print(x.view(-1).size()[0])
        length += x.view(-1).size()[0]
        sizes.append(x.view(-1).size()[0])
    sd = net.state_dict()
    for i, x in enumerate(sd.keys()):
        print(i, x, sd[x].size())
    print(length)
    splitted = eigen_vec_1.split_with_sizes(sizes)
    print(splitted)


if __name__ == "__main__":
    hessian_comp_main()
    # network_base()