import os, sys
import torch
import numpy as np

import hessian_calc
from config import Config
conf = Config()

# from IPython import embed

# power, lanczos = torch.load('tmp1')
# pd = np.std(power, axis=0)
# ld = np.std(lanczos, axis=0)

# pm = np.mean(power, axis=0)
# lm = np.mean(lanczos, axis=0)
# print(pd/pm, ld/lm)
# print(pm, lm)

# # exit()

# def load_eigenthings(fl):
#     t1 = fl.split(".P")[1]
#     t2 = t1.split("_")[0]
#     print(t2)
#     val, vec = torch.load(fl, map_location=torch.device('cpu')) # pylint: disable=no-member
#     return val, vec, t2

# target_dir = "./Hessian_Comp_2"
# file_ls = []
# for x in os.listdir(target_dir):
#     # print(x)
#     if x.endswith("eigenthings"):
#         file_ls.append(os.path.join(target_dir, x))

# power_ls, lanczos_ls = [], []
# for x in file_ls:
#     if 'lanczos' in x:
#         lanczos_ls.append(x)
#     else:
#         power_ls.append(x)

# power_dict_val = {}
# power_vec = []
# for x in power_ls:
#     val, vec, info = load_eigenthings(x)
#     vec.reverse()
#     if info not in power_dict_val:
#         power_dict_val[info] = []
#     power_vec.append(vec)
#     power_dict_val[info].append(val)

# lanczos_dict_val = {}
# lanczos_vec = []
# for x in lanczos_ls:
#     val, vec, info = load_eigenthings(x)
#     if info not in lanczos_dict_val:
#         lanczos_dict_val[info] = []
#     val = val[::-1]
#     vec.reverse()
#     if len(val) == 2:
#         lanczos_dict_val[info].append(val)
#         lanczos_vec.append(vec)

# power = power_dict_val['1.0']
# lanczos = lanczos_dict_val['1.0']

# torch.save([power, lanczos], 'tmp1')

from hessian_calc import vec2statedict_eigenthings

net = conf.net()
ls_sds = []
vec_raw = torch.load("/usr/xtmp/CSPlus/VOLDNN/Yikai/runs/1.pkl")
print(len(vec_raw))
print(len(vec_raw[0]))

for i in range(len(vec_raw)):
    vec_raw[i] = np.abs(vec_raw[i])

std = np.std(vec_raw, axis=0)
mean = np.abs(np.mean(vec_raw, axis=0))
print(np.mean(std/mean))

for i in range(1,6):
    v = torch.load("/usr/xtmp/CSPlus/VOLDNN/Yikai/runs/{}.pkl".format(i))
    tmp = []
    for k in v:
        # k = np.abs(k)
        print(np.sign(k[0]))
        tmp.append(vec2statedict_eigenthings(net, k))
    tmp.reverse()
    ls_sds.append(tmp)

def prod(v1, v2):
    print(np.linalg.norm(v1), np.linalg.norm(v2))
    return np.dot(v1, v2)

# m = np.zeros((len(vec_raw), len(vec_raw)))
# for i in range(len(vec_raw)):
#     for j in range(i):
#         print(i, j)
#         m[i][j] = prod(vec_raw[i], vec_raw[j])
# print(m)
# exit()
def analyze_sds(ls, net):
    info = {k: 0 for k in range(len(ls[0]))}
    size = 0
    for i, (var_name, var_tensor) in enumerate(net.named_parameters()):
        print(var_name)
        var_opr = {}
        for sds in ls:
            for j, sd in enumerate(sds):
                if j not in var_opr:
                    var_opr[j] = [] 
                var_opr[j].append(sd[var_name].unsqueeze(0))
                # var_opr[j].append(sd['module.' + var_name].unsqueeze(0))
        for j in sorted(list(var_opr.keys())):
            var_tensor = torch.cat(var_opr[j], dim=0)
            mean = var_tensor.mean(dim=0)
            std = var_tensor.std(dim=0)
            length = var_tensor.view(-1).size()[0]
            if j == 0:
                size += length
            dif = torch.mean(std / (mean.abs() + 1e-13))
            # print(std, mean)
            print(dif, i, j, mean.mean(), var_name)
            info[j] += length * dif.item()
            # exit()

    for j in range(len(ls[0])):
        print(j, info[j]/size)

analyze_sds(ls_sds, net)