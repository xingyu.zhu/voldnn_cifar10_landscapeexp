import os, sys, json
from copy import deepcopy
sys.path.append(os.getcwd())

import torch
from tqdm import tqdm
import numpy as np
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import datetime

from config import Config # pylint: disable=no-name-in-module
import utils
import algos.pyhessian as pyhessian
import hessian_eigenthings

from IPython import embed

def hessian_comp_main():

    conf = Config(run_number=1)
    net, device, _ = utils.prepare_net(conf.net(), conf.use_gpu)
    init_model_path = './experiment_log/run_1/models/init.pth'
    yikai_path = '/usr/xtmp/CSPlus/VOLDNN/Yikai/runs/1/models/final.pth'
    stable_path = '/usr/xtmp/CSPlus/VOLDNN/Xingyu/train_log/preliminary_exp/cifar10_resnets/resnet34vanilla_SGD_bt128_lrmodified_tfmodified_noaug/models/final.pth'
    net.load_state_dict(torch.load(stable_path))

    test_set = conf.dataset(train=True, transform=conf.transforms.rgb_normalized)
    test_loader = DataLoader(test_set, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)

    start_time = datetime.datetime.now()
    # print("xy stable power")
    print("yikai pyhessian top eigenspace power")
    eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, test_loader, conf.criterion, 2, mode="lanczos")
    # eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, test_loader, conf.criterion, 2,)
    # hessian_comp = pyhessian.hessian(net, conf.criterion, dataloader=test_loader, cuda=True)
    # eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=2)
    print(eigenvals)
    print(eigenvecs)
    torch.save(eigenvecs, 'eigenvecs_xingyu_eigenthings.tmp')
    print(datetime.datetime.now() - start_time)


def network_base():
    conf = Config(run_number=1)
    eigenvecs = torch.load('eigenvecs.tmp', map_location=torch.device('cpu'))
    # print(eigenvecs)
    # eigen_vec_1 = torch.tensor(eigenvecs[0]) # pylint: disable=not-callable
    # print(eigen_vec_1.size())
    net = conf.net()
    print(len(eigenvecs))
    eigenvec_1 = eigenvecs[0]
    sd, modified_params = vec2statedict_pyhessian(net, eigenvec_1)
    print(modified_params)
    print(sd[modified_params[-1]], eigenvec_1[-1])
    exit()
    # print(splitted)

def vec2statedict_pyhessian(net: torch.nn.Module, vec):

    assert isinstance(vec, list)
    # named_params = 
    assert len(vec) == sum(1 for _ in net.named_parameters())
    sd = deepcopy(net.state_dict())
    modified_params = []
    for i, (var_name, _) in enumerate(net.named_parameters()):
        assert sd[var_name].size() == vec[i].size(), 'shape mismatch {}'.format(var_name)
        sd[var_name] = vec[i]
        modified_params.append(var_name)
    return sd# , modified_params

def whatever():
    print(datetime.datetime.now())

if __name__ == "__main__":
    # hessian_comp_main()
    # network_base()
    whatever()