# Train and Visualize Loss Landscape of DNN

**This version already supports single GPU Computation. But the readme is not complete yet. I will have more updates on 5.29**

## Code Structure

Each directory consists of six scripts. This setting inherited the code structure in Face++ Neupeak. It is designed to make the training as easy as possible.

For training, you usually only need to modify config.py, and leave train.py unchanged.

|         Script        |                               Function                               |
|:---------------------:|:--------------------------------------------------------------------:|
| network.py            | Defines the network that is about to be trained.                     |
| config.py             | Configuration of the training, testing, and visualization procedure. |
| train.py              | Train the network.                                                     |
| test.py               | Test a specific param model with specific metrics.                                                   |
| loss_visualization.py | Visualize the loss landscape around a designated parameter set.       |
| utils.py              | Some assistive code                                                  |
| sbatch_run.py              | Helper script to submit jobs to SLURM                                                  |

The running log and parameter models are stored in the directory `./run/log` and `./run/models`. Parameter models are encoded binary forms of `OrderedDict` that can be loaded by `torch.load`.

#### Network.py

Define a network under the [scheme of pytorch](https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html). The code consists of the structure and forward propagation. For example:

```py
import torch.nn as nn
import torch.nn.functional as F

# input shape (nchw) - [n, 3, 32, 32]
# output shape - [n, 10]

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 12, 5)
        self.fc1 = nn.Linear(12 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 48)
        self.fc3 = nn.Linear(48, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 12 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
```

#### Config.py

All basic configurations of the experiment are concentrated here for easy modifications. Theoretically, config.py and network.py are the only two scripts that need to be modified when creating a new experiment.

The variables are self-explanatory from their names.

*PLEASE CHANGE* `self.base_stoage_path` *TO YOUR OWN DIRECTORY, OR IT WILL BE A MESS.*

```py
# Basic configuration
self.dataset_path = '/usr/xtmp/CSPlus/VOLDNN/Datasets/CIFAR10'

# PLEASE CHANGE TO YOUR OWN DIRECTORY
self.base_stoage_path = '/usr/xtmp/CSPlus/VOLDNN/Xingyu'
# PLEASE CHANGE TO YOUR OWN DIRECTORY

self.experiment_path = os.path.join(self.base_stoage_path, "runs/cifar10_experiment_resnet")
self.model_path = os.path.join(self.experiment_path, 'models')
self.log_path = os.path.join(self.experiment_path, 'log')
self.classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

self.net = ResNet34

# Training
self.transform = transforms.Compose(
    [transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
)
self.trainset_info = 'CIFAR10Train'
if data_load:
    self.trainset = CIFAR10(root=self.dataset_path, train=True, download=True, transform=self.transform)
self.train_provider_count = 4
self.training_batchsize = 128
self.epoch_num = 90
self.snapshot_freq = 10

self.criterion = nn.CrossEntropyLoss()

# Testing
self.testset_info = 'CIFAR10Test'
if data_load:
    self.testset = CIFAR10(root=self.dataset_path, train=False, download=True, transform=self.transform)
self.test_provider_count = 4
self.testing_batchsize = 128

self.local_explog_softlink = "./experiment_log"

if data_load:
    os.system("rm {}".format(self.local_explog_softlink))
    os.system("ln -s {} {}".format(self.experiment_path, self.local_explog_softlink))

def optimizer_conf(self, net):
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9, weight_decay=1e-5)
    return optimizer

def lr_scheduler_conf(self, optimizer):
    lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 30, gamma=0.1)
    return lr_scheduler
```

### Training: Train.py

This is the code we run when we perform training jobs.
When training a completely new experiment, we simply run

```shell
python3 train.py
```

When the code runs properly, we should see something like this

![running_log](https://gitlab.cs.duke.edu/xingyu.zhu/torch_cifar10_test/-/raw/master/imgs/training_log.png)

When the experiment is killed for some reason or you want to restart training for some reason. You can run with the following arguments.

**Continue training from the last snapshot**

```shell
python3 train.py -c
```

**Overwrite everything and restart training from scratch**

```shell
python3 train.py -o
```

**Load a specific param model** (you need to specify what epoch is that param model). For example, if I want to start training from  `./runs/cifar10_experiment_1/models/epoch9.pth`, which is a param snapshot from epoch `9`, then I run

```shell
python3 train.py -l=./runs/cifar10_experiment_1/models/epoch9.pth -lep=9
```

All param snapshots are stored in `config.model_path` in the format of `epochxxx.pth`, `config.model_path` is defined in `config.py`. You may also adjust the frequency of the snapshot in `config.py`

### Testing: test.py

If you want to test a specific model on the test set (e.g.epoch6.pth), you run

```shell
python3 test.py -m=epoch6.pth
```

Note that if that model is in  `config.model_path`, you do not need the complete path (but complete path is still supported if you want to test param models else where). 

### Loss Landscape Visualization

This is an implementation of https://arxiv.org/pdf/1712.09913.pdf

By default, the code can make 2D (two directional vector dimension and a loss dimension, so 3D in plot) visualization of loss landscape (1D will be supported tomorrow, but it might be useless in the analysis, the code is actually capable of computing the loss landscape of any dimension, but we live in a 3D space).

The code needs two arguments, a param model path (just like the one in test) and on a config array such as `[[-1,1,11,1],[-1,1,11,2]`. This can be intepreted as calculating the loss landscape in a "2D subspace" of the parameter space which is "centered" at the given param model/snapshot.

The config array is of the form
`[[start1, end1. number1, random_seed1],[start2, end2, number2, random_seed2]`
The subspace is spanned by two random "vectors", with the specific random seed. The discrete sampling is defined by start, end, and number like `np.linspace`.

To visualize a landscape, you run:

```shell
python3 loss_visualization.py -C="[[-1,1,21,1],[-1,1,21,2]]"
```

And will get an output like this

![sample](https://gitlab.cs.duke.edu/xingyu.zhu/torch_cifar10_test/-/raw/master/imgs/sample_simplenetwork.png)
