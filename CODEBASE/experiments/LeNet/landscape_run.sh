#!/bin/bash
echo "Running landscape on run#" $1

python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_2d.py -run=$(($1)) -m=final.pth -r=1 -d=15 -i -w -E2D"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_2d.py -run=$(($1)) -m=final.pth -r=0.1 -d=15 -i -w -E2D"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_2d.py -run=$(($1)) -m=final.pth -r=0.3 -d=15 -i -w -E2D"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_2d.py -run=$(($1)) -m=final.pth -r=3 -d=15 -i -w -E2D"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_2d.py -run=$(($1)) -m=final.pth -r=10 -d=15 -i -w -E2D"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_1d.py -run=$(($1)) -m=final.pth -r=1 -d=15 -i -w -E1D -vecnum=10"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_1d.py -run=$(($1)) -m=final.pth -r=0.1 -d=15 -i -w -E1D -vecnum=10"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_1d.py -run=$(($1)) -m=final.pth -r=0.3 -d=15 -i -w -E1D -vecnum=10"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_1d.py -run=$(($1)) -m=final.pth -r=3 -d=15 -i -w -E1D -vecnum=10"
python3 sbatch_run.py -x="train-s" -r="python3 loss_visualization_1d.py -run=$(($1)) -m=final.pth -r=10 -d=15 -i -w -E1D -vecnum=10"