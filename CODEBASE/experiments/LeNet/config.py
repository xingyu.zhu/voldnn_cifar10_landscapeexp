import os
import sys
import datetime, time
from torch import optim, nn
import torchvision.transforms as transforms
from torchvision.datasets import CIFAR10

import numpy as np
from networks.LeNet import Net as LeNet

class Config():

    def __init__(self, run_number=-1):

        # Basic configuration
        self.dataset_path = '/usr/xtmp/CSPlus/VOLDNN/Datasets/CIFAR10'

        self.base_storage_path = '/usr/xtmp/CSPlus/VOLDNN/Shared/train_log/cifar10_landscape_exp'
        self.experiment_path = os.path.join(self.base_storage_path, os.getcwd().split('/')[-3], os.getcwd().split('/')[-1], "run_{}".format(run_number))
        self.experiment_path_root = os.path.join(self.base_storage_path, os.getcwd().split('/')[-3], os.getcwd().split('/')[-1])
        
        self.vis_dir = os.path.join(self.experiment_path, 'vis')
        self.model_path = os.path.join(self.experiment_path, 'models')
        self.log_path = os.path.join(self.experiment_path, 'log')

        self.run_number = run_number
        self.transforms = self.Transforms()
        self.datasets = self.Datasets()
        
        self.classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
        self.use_gpu = True

        # Basic Information
        self.net = LeNet
        self.dataset = self.datasets.dataset_cifar10

        # Training
        self.trainset_info = 'CIFAR10Train'
        self.train_provider_count = 4
        self.training_batchsize = 128
        
        self.epoch_num = 300
        self.snapshot_freq = 10

        self.criterion = nn.CrossEntropyLoss()
        self.stop_by_criterion = 1e-6

        # Testing
        self.testset_info = 'CIFAR10Test'
        self.test_provider_count = 4
        self.testing_batchsize = 128

        self.train_transform = self.transforms.rgb_normalized
        self.test_transform = self.transforms.rgb_normalized

        self.local_explog_softlink = "./experiment_log"
        
        # Creating Symlink to training log under the shared folder.
        if run_number > 0:
            if os.path.islink(self.local_explog_softlink):
                if os.readlink(self.local_explog_softlink) != self.experiment_path_root:
                    os.system("rm -f {}".format(self.local_explog_softlink))
                    time.sleep(1)
                    os.system("ln -s {} {}".format(self.experiment_path_root, self.local_explog_softlink))
                    print("trainlog symlink incorrect, recreated")
                else:
                    print("symlink created already")
            else:
                os.system("ln -s {} {}".format(self.experiment_path_root, self.local_explog_softlink))
                print("trainlog symlink created")
            
    def optimizer_conf(self, net):
        # optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9)
        optimizer = optim.SGD(net.parameters(), lr=0.1)
        return optimizer
    
    def lr_scheduler_conf(self, optimizer):
        # lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 50, gamma=0.1)
        lr_scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=np.power(0.1, 1/50), last_epoch=-1)
        return lr_scheduler
    
    def dataset_cifar10(self, train, transform):
        return CIFAR10(root=self.dataset_path, train=train, download=True, transform=transform)
    
    class Transforms():

        def __init__(self):
            self.naive_transform = transforms.Compose(
                [transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
            )
            
            self.rgb_normalized_crop_flip = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ])

            self.rgb_normalized = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ])
    
    class Datasets():

        def __init__(self):
            self.choices = ['CIFAR10']

        def dataset_cifar10(self, train, transform):
            return CIFAR10(root=Config().dataset_path, train=train, download=True, transform=transform)

