import os, sys
sys.path.append(os.getcwd())

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.backends.cudnn as cudnn

import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from tqdm import tqdm

from config import Config # pylint: disable=no-name-in-module
from test import test_model
import utils

from hessian_calc import pyhessian_eigenthings

from os.path import join as pathjoin
import shutil, argparse, datetime, json
from IPython import embed
import pynvml

# == Arguments ==
parser = argparse.ArgumentParser()
parser.add_argument('-run', type=int, help='The i-th run of the experiment (default=1)', default=1)

g = parser.add_mutually_exclusive_group(required=False)
g.add_argument('-l', type=str,help='continue training from a specific model')
g.add_argument('-c', action='store_true', help='continue training from the latest model by the default scheme')
g.add_argument('-o', action='store_true', help='Overwrite everything and start training')

parser.add_argument('-lep', type=int, help='the epoch to start from')
parser.add_argument('-v', action='store_false', help='validate along the training process on the test set?')
parser.add_argument('-tfrefresh', action='store_true',help='Remove existing tensorboard records')

args = parser.parse_args()
validation_arg = args.v

conf = Config(run_number=args.run)

assert (((args.l is not None) and args.c) or ((args.l is not None) and args.o) or (args.c and args.o)) is not True, 'You may only choose one among continue, overwrite, and load.'

print('Performing experiments at {}'.format(os.getcwd()))
print('Run #{}'.format(args.run))
print('Log will be saved at {}'.format(conf.experiment_path))

start_epoch = -1
initial_net_dict = None

if args.l is not None:
    start_epoch = args.lep - 1
    assert args.lep is not None, 'You must specify a start epoch in argument -lep (if training from scratch, type -lep=0)'
    assert os.path.isfile(args.l), 'Invalid model'
    print('Loading {}, starting at epoch {}').format(args.l, args.lep)
    initial_net_dict = (torch.load(args.l))

elif os.path.isdir(conf.experiment_path):
    print('Previous Training Record Exists.')
    # Continue training by loading latest.pthl from the models directory
    if args.c:
        latest_model = pathjoin(conf.model_path, 'latest.pthl')
        assert os.path.isfile(latest_model), 'no valid latest.pthl'
        meta = torch.load(latest_model)
        assert isinstance(meta, dict)
        start_epoch = meta['epoch']
        print('Loading {}/latest.pthl, starting at epoch {}'.format(conf.model_path, start_epoch))
        initial_net_dict = meta['state_dict']
    
    # Overwrite everything
    elif args.o:
        print('Overwriting everything.')
        shutil.rmtree(conf.experiment_path)
        for direc in [conf.experiment_path, conf.model_path, conf.log_path]:
            os.mkdir(direc)
    
    else:
        print('Please either choose overwrite(-o), load(-l & -leq), or continue(-c) in argument')
        exit()

if args.tfrefresh:
    if os.path.isdir(conf.log_path):
        shutil.rmtree(conf.log_path)

for dirc in [conf.experiment_path, conf.model_path, conf.log_path, conf.vis_dir]:
    if not os.path.isdir(dirc):
        print(dirc)
        os.makedirs(dirc)

# == Initializing Network and Datasets ==
print('Initializing Network')
net, device, handle = utils.prepare_net(conf.net(), conf.use_gpu)
if initial_net_dict is not None:
    net.load_state_dict(initial_net_dict)

train_set = conf.dataset(train=True, transform=conf.train_transform)
train_loader = DataLoader(train_set, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)

if validation_arg:
    val_set = conf.dataset(train=False, transform=conf.test_transform)
    val_loader = DataLoader(val_set, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)

# Tensorboard Log Writer
writer = SummaryWriter(conf.log_path)

# Visualizing Structure of the Network
dataiter = iter(train_loader)
images, labels = dataiter.next()
try:
    writer.add_graph(net, images.to(device))
except:
    print('failed visualizing network structure in tensorboard.')
    # from torchviz import make_dot
    # images = images.to(device)
    # tmp_outputs = net(images)
    # make_dot(tmp_outputs).render("network_structure", format="jpg")
    # os.system('rm -f network_structure')
    pass

criterion = conf.criterion
optimizer = conf.optimizer_conf(net)
scheduler = conf.lr_scheduler_conf(optimizer)
if start_epoch == -1:
    torch.save(net.state_dict(), pathjoin(conf.model_path, 'init.pth'))

# Start Training    
print('Start Training: {}'.format(datetime.datetime.now()))
for epoch in range(conf.epoch_num):  # loop over the dataset multiple times

    if epoch <= start_epoch:
        scheduler.step()
        continue

    running_loss = 0.0
    start_time = datetime.datetime.now()

    for i, data in enumerate(train_loader, 0):

        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        optimizer.zero_grad()

        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # scheduler.step()
        step_count = epoch * len(train_loader) * conf.training_batchsize + i * conf.training_batchsize
        running_loss += loss.item()
        update_freq = 100

        # Logging Training Loss
        if i % update_freq == update_freq - 1:
            avg_loss = running_loss / update_freq
            writer.add_scalar('train_loss', avg_loss, step_count)
            writer.add_scalar('lr', scheduler.get_last_lr()[0], step_count)
            running_loss = 0.0
        
        # Logging GPU Performance
        if device != 'cpu':
            gpu_update_freq = 250
            if i % gpu_update_freq == update_freq - 1:
                mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
                load_info = pynvml.nvmlDeviceGetUtilizationRates(handle)
                writer.add_scalar('GPURAM_usage', mem_info.used / mem_info.total, step_count)
                writer.add_scalar('GPU_usage', load_info.gpu, step_count)
    
    # Creating Snapshots
    if epoch % conf.snapshot_freq == conf.snapshot_freq - 1:
        torch.save(net.state_dict(), pathjoin(conf.model_path, 'epoch{}.pth'.format(epoch + 1)))
    torch.save({'state_dict':net.state_dict(), 'epoch':epoch}, pathjoin(conf.model_path, 'latest.pthl'))
    
    # Printing Train Log
    end_time = datetime.datetime.now()
    print("Epoch {} Training Finished\t Training Loss {}\t Runtime: {}\t Logtime: {}".format(epoch + 1, avg_loss, end_time - start_time, end_time))
    
    # Validation Process
    if validation_arg:
        start_time = datetime.datetime.now()
        val_meta = test_model(net, val_loader, conf.criterion, device, progress_bar=False)
        val_loss = val_meta['loss']
        writer.add_scalar('val_loss', val_loss, step_count)
        end_time = datetime.datetime.now()
        print("Epoch {} Validation Finished\t Validation Loss {}\t Runtime: {}\t Logtime: {}".format(epoch + 1, val_loss, end_time - start_time, end_time))
    
    # Early stopping by criterion
    if avg_loss < conf.stop_by_criterion:
        print("Criterion less than designated value, abort training.")
        break
    scheduler.step()

print('Finished Training. The last snapshot saved to final.pth')
torch.save(net.state_dict(), pathjoin(conf.model_path, 'final.pth'))

# Evaluation After Training
print('evaluating results')
test_set = conf.dataset(train=False, transform=conf.test_transform)
test_loader = DataLoader(test_set, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)

train_eval_result = test_model(net, train_loader, conf.criterion, device, progress_bar=False)
test_eval_result = test_model(net, test_loader, conf.criterion, device, progress_bar=False)

exp_meta_path = os.path.join(conf.experiment_path, 'exp_log.json')
meta = dict()
if os.path.isfile(exp_meta_path):
    meta_io = open(exp_meta_path, 'r')
    meta = json.load(meta_io)
    meta_io.close()
meta[pathjoin(conf.model_path, 'final.pth')] = {'test_err': test_eval_result, 'train_err': train_eval_result, 'generalization_gap': {k: test_eval_result[k] - train_eval_result[k] for k in test_eval_result}}
meta_writer = open(exp_meta_path, 'w')
json.dump(meta, meta_writer)

print("Running Random Landscape")
os.system("bash ./landscape_run.sh {}".format(args.run))

os.system("""python3 sbatch_run.py  --exclude="gpu-compute[1-3],linux56" -x="train-s" -r="python3 hessian_calc.py -ET -method='lanczos' -topn=10 -run={}""".format(args.run))
