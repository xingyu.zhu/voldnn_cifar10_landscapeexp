import os, sys
sys.path.append(os.getcwd())

import torch
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
import numpy as np
import pynvml

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import plotly.graph_objects as go
import plotly

import hashlib, json, argparse, os, datetime
from copy import deepcopy
from tqdm import tqdm
from termcolor import colored
from os.path import join as pathjoin
from collections import OrderedDict

import utils
from config import Config # pylint: disable=no-name-in-module
from test import test_model

conf = Config()
# conf = Config()

def loss_landscape_nd(net, dataloader, criterion, configs, device):
    """
    Config: a n x 4 list
    each row indicates start, end, number of points, random seed (None for random)
    """
        
    sd_base = deepcopy(net.state_dict())

    sd_v_ls, grid_ls = [], []
    for i in range(len(configs)):
        print(configs[i][:-1])
        start, end, num, vec = configs[i]
        grid_ls.append(np.linspace(start, end, num))
        sd_v_ls.append(vec)
    
    grid = utils.cartesian_product_itertools(grid_ls)
    loss_landscape = np.zeros(len(grid))

    for i, s in enumerate(grid):
        begin_time = datetime.datetime.now()
        sd = sd_base
        for j in range(len(configs)):
            sd = utils.state_dict_add(sd, utils.state_dict_scalar_mul(sd_v_ls[j], s[j]))
        net.load_state_dict(sd)
        sd_loss = test_model(net, dataloader, criterion, device, progress_bar=False)
        loss_landscape[i] = sd_loss['loss']
        end_time = datetime.datetime.now()

        print("# {}/{}\t loss: {}\t took {}".format(i + 1, len(grid), sd_loss['loss'], end_time - begin_time))
    
    return loss_landscape, grid, grid_ls

def reshape2nd(landscape, grid, grid_ls):
    grid_shape = [len(x) for x in grid_ls]
    grid = grid.reshape(grid_shape + [len(grid_ls)])
    landscape = landscape.reshape(grid_shape)
    return landscape, grid
    
def mpl3d_out(landscape, grid, grid_ls):
    landscape, grid = reshape2nd(landscape, grid, grid_ls)
    assert grid.shape[-1] == 2
    grid = [grid[:, :, 0], grid[:, :, 1]]
    assert grid[0].shape == landscape.shape
    return landscape, grid

def visualize_landscape_1d(landscape, grid):

    plt.plot(grid, landscape)
    plt.show()

def create_logname(model_path, length, density, using_testset, tag):

    model_abbrev = '-'.join(model_path.split('/')[-4:])
    if using_testset:
        dataset_desc = conf.testset_info
    else:
        dataset_desc = conf.trainset_info
    name = '{}_{}__{}__R{}__D{}.landscape'.format(tag, model_abbrev, dataset_desc, length, density)
    return name

def compute_2d_landscape(net, model_path, length, density, using_testset):

    net_base, device, _ = utils.prepare_net(net, conf.use_gpu)

    start_time = datetime.datetime.now()
    if using_testset:
        print("using test set")
        test_set = conf.dataset(train=False, transform=conf.test_transform)
        dataloader = DataLoader(test_set, batch_size=conf.testing_batchsize, shuffle=False, num_workers=conf.test_provider_count)
    else:
        print("using train set")
        train_set = conf.dataset(train=True, transform=conf.test_transform)
        dataloader = DataLoader(train_set, batch_size=conf.training_batchsize, shuffle=False, num_workers=conf.test_provider_count)

    param_model = torch.load(model_path)
    net_base.load_state_dict(param_model)

    sd_base = deepcopy(net_base.state_dict())
    d_vec1 = utils.gaussian_random_direction_filterwise_rescaled(sd_base, random_seed=1)
    d_vec2 = utils.gaussian_random_direction_filterwise_rescaled(sd_base, random_seed=2)

    configs = [[-length, length, density, d_vec1], [-length, length, density, d_vec2]]
    loss_landscape, grid, grid_ls = loss_landscape_nd(net_base, dataloader, conf.criterion, configs, device)
    print("Computing done, Runtime: {}".format(datetime.datetime.now() - start_time))

    return loss_landscape, grid, grid_ls

def load_from_landscape(load_landscape_path):
    
    if '/' not in load_landscape_path:
        load_landscape_path = os.path.join(conf.vis_dir, load_landscape_path)
    assert os.path.isfile(load_landscape_path), 'Invalid landscape path by the -L argument'
    loss_landscape, grid, grid_ls = torch.load(load_landscape_path)
    return loss_landscape, grid, grid_ls

    # loss_landscape, grid = torch.load(load_landscape_path)
    # return loss_landscape, grid

def visualize_landscape_2d_mpl(landscape_2d, grid_2d, grid_ls, info='loss landscape', save_dirc=None, show=False):

    print('visualize using Matplotlib')

    landscape_2d_mpl, grid_2d_mpl = mpl3d_out(landscape_2d, grid_2d, grid_ls)
    x, y = grid_2d_mpl
    if not show:
        mpl.use('Agg')

    print('Figure initialized')

    fig = plt.figure()
    print('Start Initializing')
    ax = fig.gca(projection='3d')
    print('Start plotting')
    ax.plot_surface(x, y, landscape_2d_mpl, cmap=cm.coolwarm, linewidth=0, antialiased=False) # pylint: disable=maybe-no-member
    if save_dirc is not None:
        print('saving to {}'.format(save_dirc))
        plt.savefig(save_dirc, dpi=220, bbox_inches = 'tight')
    if show:
        plt.show()

def visualize_landscape_2d_plotly(landscape_2d, grid_2d, grid_ls, save_html=None, save_img=None, show=False, info='loss landscape'):

    min_z, max_z = landscape_2d.min(), landscape_2d.max()
    print('Initializing plotly')
    landscape_2d, grid_2d = reshape2nd(landscape_2d, grid_2d, grid_ls)
    fig = go.Figure(go.Surface(
        contours = {
        "x": {"show": True, "start": grid_ls[0][0], "end": grid_ls[0][-1], "size": (grid_ls[0][-1] - grid_ls[0][0]) / 20, "color":"grey", "highlight":True, "width":10},
        "y": {"show": True, "start": grid_ls[0][0], "end": grid_ls[0][-1], "size": (grid_ls[1][-1] - grid_ls[1][0]) / 20, "color":"grey", "highlight":True, "width":10},
        "z": {"show": True, "start": min_z, "end": max_z, "size": (max_z - min_z) / 20, "color":"white", "highlight":True, "width":16, "usecolormap":True, "project_z":True}
        },
        x = grid_ls[0],
        y = grid_ls[1],
        z = landscape_2d,
        ))

    fig.update_layout(
        title="""<a href="https://www2.cs.duke.edu/~xz231">Back to homepage</a>""",
    )
    if save_html is not None:
        print('exporting html')
        plotly.offline.plot(fig, filename=save_html, auto_open=False)
    if show:
        fig.show()

def main_2d_random_filterwise_rescaled():

    parser = argparse.ArgumentParser()
    parser.add_argument('-run', type=int, help='The number of run', default=1)
    g = parser.add_mutually_exclusive_group(required=True)

    g.add_argument('-m', type=str, help='The param model as the center of the plot')
    g.add_argument('-l', type=str, help='Load specific output')
    g.add_argument('-x', action='store_true', help='Visualize new existing .landscape files')
    g.add_argument('-xx', action='store_true', help='Visualize all existing .landscape files')

    parser.add_argument('-o', action='store_true', help='Overwrite existing result or not')
    parser.add_argument('-i', action='store_true', help='Output image of landscape?')
    parser.add_argument('-w', action='store_true', help='Output interactive webpage (plotly) of landscape?')
    
    parser.add_argument('-r', type=float, default=0.1, help='range of landscape')
    parser.add_argument('-d', type=int, default=15, help='mesh density')
    parser.add_argument('-t', action='store_true', help='Using Test Set')
    
    args = parser.parse_args()
    
    global conf
    conf = Config(run_number=args.run)

    if not os.path.isdir(conf.vis_dir):
        os.makedirs(conf.vis_dir)
    
    length, density = args.r, args.d
    using_testset = args.t
    load_landscape_path = args.l
    model_path = args.m
    out_img, out_html = args.i, args.w
    force_overwrite = args.o

    print(args)

    if args.x or args.xx:
        fl_ls = []
        for x in os.listdir(conf.vis_dir):
            if x.endswith('.landscape'):
                complete_path = pathjoin(conf.vis_dir, x)
                if args.xx:
                    if not os.path.isfile(complete_path + '.jpg'):
                        fl_ls.append(complete_path)
                else:
                    fl_ls.append(complete_path)
        print(fl_ls)

        for i, x_path in enumerate(fl_ls):
            print("{}/{} Dealing with {}".format(i + 1, len(fl_ls), x_path))
            loss_landscape, grid, grid_ls = torch.load(x_path)
            visualize_landscape_2d_mpl(loss_landscape, grid, grid_ls, save_dirc=x_path + '.jpg')
            visualize_landscape_2d_plotly(loss_landscape, grid, grid_ls, save_html=x_path + '.html')
        return

    # Read directedly from a .landscape file
    if load_landscape_path is not None:
        loss_landscape, grid, grid_ls = load_from_landscape(load_landscape_path)
        log_path = load_landscape_path

    else:
        assert model_path is not None, 'You must specify one among -L and -M'
        if '/' not in model_path:
            model_path = os.path.join(conf.model_path, model_path)
        assert os.path.isfile(model_path), 'Invalid model path by the -M argument'

        log_name = create_logname(model_path, length, density, using_testset, "2ddomeigenspace")
        log_path = os.path.join(conf.vis_dir, log_name)

        if os.path.isfile(log_path):
            print('This model has been previously calculated.')
            if not force_overwrite:
                print('Loading from previous records.')
                loss_landscape, grid, grid_ls = load_from_landscape(log_path)
            else:
                print('overwriting existing records.')
                net = conf.net()
                loss_landscape, grid, grid_ls = compute_2d_landscape(net, model_path, length, density, using_testset)
                torch.save([loss_landscape, grid, grid_ls], log_path)
                print('Landscape saved to {}'.format(log_path))
        else:
            net = conf.net()
            print('No previous record exists')
            loss_landscape, grid, grid_ls = compute_2d_landscape(net, model_path, length, density, using_testset)
            torch.save([loss_landscape, grid, grid_ls], log_path)
            print('Landscape saved to {}'.format(log_path))

    if out_img:
        image_path = log_path + '.jpg'
        visualize_landscape_2d_mpl(loss_landscape, grid, grid_ls, save_dirc=image_path)
    
    if out_html:
        html_path = log_path + '.html'
        visualize_landscape_2d_plotly(loss_landscape, grid, grid_ls, save_html=html_path)

if __name__ == "__main__":
    main_2d_random_filterwise_rescaled()



    
    
    # visualize_landscape_2d_mpl(configs, model, device, compute_only=args.G, force_overwrite=args.O, using_testset=args.T)
